# INSTALLATION

Copier le dossier *database* dans le projet.

# UTILISATION

## Dans phpMyAdmin :

Le titre des colonnes doivent figurer dans les commentaires

Les identifiants des tables seront toujours nommés _id_

## Au début du fichier :

    require("database/Database.php");
    $database = new Database();
    $database->doActions("<TABLE>");

La fonction `doActions` doit être présente afin de recevoir les variables postées.

## Où vous shouhaitez afficher le CRUD:

    database->makeCrud("<TABLE>");

# OPTIONS


La fonction `makeCrud` peut contenir juste le nom de la table, 
mais peut-être personnalisée avec ces options :

## Mettre sa propre requête SQL :

### En une seule ligne

    $database->makeCrud("client",[
        'sql' => 'SELECT id,client_id FROM client',
    ]);

### En plusieurs lignes

    $database->makeCrud("commande",[
        'sql' => "
    SELECT ligne_commande.id,ligne_commande.date_livraison,ligne_commande.adresse_livraison,client.nom,client.prenom
    FROM ligne_commande
    INNER JOIN commande ON ligne_commande.id = commande.id
    INNER JOIN client ON commande.client_id = client.id
        ",
    ]);
## Ajouter des actions après le bouton _Edit_

Par défaut, l'identifiant _id_ sera toujours ajouté au lien.

### Pour un lien avec la table "commandes" :

    $database->makeCrud("client",[
        'actions' => [
            'commande' => 'voir les commandes',
            ...
        ],
    ]);

### Pour un lien vers un autre fichier :

    $database->makeCrud("client",[
        'actions' => [
            'lien.php' => 'voir les commandes',
            ...
        ],
    ]);