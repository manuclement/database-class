<?php
/**
 * @var array $columns
 * @var string $table
 * @var array $jsIds
 * @var array $tables
 * @var array $titlesAndValues
 */
?>
<div id="crud-add-<?= $table; ?>" class="crud-add">
    <form method="post">
        <input type="hidden" name="id" value="new">
        <?php foreach ($columns as $column) : ?>
            <?php if ($column["name"] !== "id") : ?>
                <div class="popup-title">

                    <?php
                    if (in_array(str_replace("_id","",$column["name"]),$jsIds)) {
                        $tableindex = str_replace("_id","",$column["name"]);
                        echo '<select class="swal2-select" name="'.$column["name"].'">';
                        foreach ($tables[$tableindex] as $thistable) {
                            echo '<option id="'.$thistable[0].'" value="'.$thistable[0].'">'.$thistable[1].'</option>';
                        }

                        echo '</select>';
                    }
                    else {
                        $required = $column["required"] ? " required" : "";
                        echo DatabaseGetInputTypeToString($column["type"],$column["name"],$required,$column["title"],"add");}?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </form>
</div>