<?php

class Connection {
    public function __construct() {
        /**
         * @var array $db_connections
         */

//        DB CONNECTION

        if (file_exists(__DIR__.'/.env')) {
            $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
            $dotenv->load();
            var_dump($_ENV);
        }

        else {
            require("database/connection.php");
            $server_host = $_SERVER['HTTP_HOST'];
            $server_port = -1;
            $db_url = null;
            $db_username = null;
            $db_password = null;
            if (strrpos($server_host, ":") !== false) {
                $en = explode(":", $server_host);
                $server_host = $en[0];
                $server_port = $en[1];
            }
            foreach ($db_connections as $connection) {
                if ($connection["server_host"] == $server_host && $connection["server_port"] == $server_port) {
                    $this->db_name = $connection['db_name'];
                    $db_url = $connection['db_port'] < 0 ? "mysql:host=" . $connection['db_host'] . ";dbname=" . $this->db_name : "mysql:host=" . $connection['db_host'] . ";port=" . $connection['db_port'] . ";dbname=" . $connection['db_name'];
                    $db_username = $connection['db_username'];
                    $db_password = $connection['db_password'];
                    break;
                }
            }
        }

        try {
            //$this->conn = new PDO("mysql:host=" . $host . ";dbname=" . $database_name, $username, $password);
            $this->conn = new PDO($db_url, $db_username, $db_password);
            $this->conn->exec("set names utf8");
        } catch (PDOException $exception) {
            die("Database could not be connected: " . $exception->getMessage());
        }


//        FIND LOCATION HREF

        $this->location = "Location: " . ltrim($_SERVER['REQUEST_URI'], "/");
        if ($this->location == "Location: ") {
            $this->location = "Location: http://$server_host";
            if ($connection["server_port"] > -1)
                $this->location .= ":" . $connection["server_port"];
        }
        $this->href = str_replace("Location: ", "", $this->location);

    }
}