<?php


use JetBrains\PhpStorm\ArrayShape;

class Database
{

    public function __construct()
    {
        /**
         * @var array $db_connections
         */

//        DB CONNECTION
        $server_host = $_SERVER['HTTP_HOST'];
        if (file_exists($_SERVER["DOCUMENT_ROOT"].'/.env')) {
            require '../vendor/autoload.php';
            $dotenv = Dotenv\Dotenv::createImmutable($_SERVER["DOCUMENT_ROOT"]);
            $dotenv->load();
            $connection = [
                "db_name" => $_ENV["db_name"],
                "db_port" => $_ENV['db_port'],
                "db_username" => $_ENV["db_username"],
                "db_password" => $_ENV["db_password"],
                "server_port" => $_ENV["server_port"]
            ];

            /*$db_name = $connection['db_name'];
            $db_url = $connection['db_port'] < 0 ? "mysql:host=" . $connection['db_host'] . ";dbname=" . $db_name : "mysql:host=" . $connection['db_host'] . ";port=" . $connection['db_port'] . ";dbname=" . $connection['db_name'];
            $db_username = $connection['db_username'];
            $db_password = $connection['db_password'];*/


            $db_name = $_ENV["db_name"];
            $db_url = $_ENV['db_port'] < 0 ? "mysql:host=" . $_ENV['db_host'] . ";dbname=" . $db_name : "mysql:host=" . $_ENV['db_host'] . ";port=" . $_ENV['db_port'] . ";dbname=" . $_ENV['db_name'];;
            $db_username = $_ENV["db_username"];
            $db_password = $_ENV["db_password"];

        }

        else {
            require("database/connection.php");
            $server_port = -1;
            $db_url = null;
            $db_username = null;
            $db_password = null;
            if (strrpos($server_host, ":") !== false) {
                $en = explode(":", $server_host);
                $server_host = $en[0];
                $server_port = $en[1];
            }
            foreach ($db_connections as $connection) {
                if ($connection["server_host"] == $server_host && $connection["server_port"] == $server_port) {
                    $db_name = $connection['db_name'];
                    $db_url = $connection['db_port'] < 0 ? "mysql:host=" . $connection['db_host'] . ";dbname=" . $db_name : "mysql:host=" . $connection['db_host'] . ";port=" . $connection['db_port'] . ";dbname=" . $connection['db_name'];
                    $db_username = $connection['db_username'];
                    $db_password = $connection['db_password'];
                    break;
                }
            }
        }

        try {
            //$this->conn = new PDO("mysql:host=" . $host . ";dbname=" . $database_name, $username, $password);
            $this->conn = new PDO($db_url, $db_username, $db_password);
            $this->conn->exec("set names utf8");
        } catch (PDOException $exception) {
            die("Database could not be connected: " . $exception->getMessage());
        }


//        FIND LOCATION HREF

        $this->location = "Location: " . ltrim($_SERVER['REQUEST_URI'], "/");
        if ($this->location == "Location: ") {
            $this->location = "Location: http://$server_host";
            if ($connection["server_port"] > -1)
                $this->location .= ":" . $connection["server_port"];
        }
        $this->href = str_replace("Location: ", "", $this->location);



//        MISC VARS

        $this->correspondenceTable = [];
        $this->tableNames = $this->getTablesInDatabase($db_name);
        //var_dump($this->tableNames);
        //var_dump($this->location);
        /*if ($connection["server_port"] > -1)
            $this->location = "Location: ".ltrim($_SERVER['REQUEST_URI'],"/").":".$connection["server_port"];
        var_dump($this->location);*/
        $this->is_crudJScalled = false;
        $this->jsonJS = [];
        $this->tableSelectInput = [];
        $this->tables = [];
    }

    public function getConnection () {

    }

    public function makeCrud($table,$options = [])
    {
        $table_id = isset($_GET["t"]) ? intval($_GET["t"]) : null;
        $index = isset($_GET["from"]) ? $_GET["from"]."_id" : null;
        $id = $_GET["$index"] ?? null;
        if (!is_null($table_id) && is_int($table_id)) {
            if ($table_id < count($this->tableNames)) {
                $table = $this->tableNames[$table_id];
                $options["sql"] = 'SELECT * FROM '.$table.' WHERE '.$index.'='.$id;
            }
        }

        $actions = [];
        if (isset($options["actions"])) {
            if (is_array($options["actions"])) {
                foreach ($options["actions"] as $key => $value) {
                    if (strrpos($key, ".") !== false) {
                        $actions[] = [
                            'html' => '<a class="newaction" href="'.$key.'?'.$table.'_id=[ID]&from='.$table.'">'.$value.'</a>',
                            'id_index' => $table.'_id',
                        ];
                    }

                    if (in_array($key,$this->tableNames)) {
                        $index = array_search($key,$this->tableNames);
                        if ($key != $table) {
                            $actions[] = [
                                'html' => '<a class="newaction" href="?t='.$index.'&'.$table.'_id=[ID]&from='.$table.'">'.$value.'</a>',
                                'id_index' => $table.'_id',
                            ];
                        }
                    }
                }
            }
        }

        $sql = $options["sql"] ?? "SELECT * FROM $table";
        $sql = str_replace("\n"," ",$sql);
        $sql = str_replace("  "," ",$sql);
        $sql = ltrim($sql," ");
        $select = explode(",",explode(" ",$sql)[1]);


//        NEEDED FOR THE REQUIREMENT
        $hasCrudActions = true; //strrpos($sql, "INNER JOIN") === false;
            //$columns = $this->getColumns($table,$select);
            //$tableValues = isset($options["sql"]) ? $this->findAll($table,$options["sql"]) : $this->findAll($table);




        $hidden = $options["hide"] ?? [];
        if (!is_array($hidden)) {
            if (strrpos($options["hide"],',') !== false) {
                $hidden = explode(",",$options["hide"]);
            }
            else {
                $hidden = [$hidden];
            }
        }

        $titlesAndValues = $this->getTableResults(str_replace("*","$table.*",$sql),$hidden);
        $javascripts = $this->jsonJS;
        $jsIds = $this->tableSelectInput;
        $tables = $this->tables;
        require("database/template.php");
        if (!$this->is_crudJScalled) {
            $this->is_crudJScalled = true;
            echo '<script src="database/crud.js"></script>';
        }

    }

    public function checkSession() {
        if (!in_array('sessions',$this->tableNames)) {
            $this->execute('CREATE TABLE sessions (id int,last_access datetime, user_id varchar(255))',[]);
        }
        if (isset($_GET['logout'])) {
            setcookie('loggedin');
            header($this->location);

        }

    }

    public function getColumnInfos($sql,$hidden): array
    {

        $selectString = explode(" ",$sql)[1];

        $selects = strrpos($selectString,",") === false ? [$selectString] : explode(",",$selectString);
        $tables = [];

        $sql_tables = [];

        if (strrpos($sql,"INNER JOIN") !== false) {
            $fake_sql = str_replace("INNER JOIN","INNER_JOIN",$sql);
            $sql_elements = explode(" ",$fake_sql);

            for ($i=0;$i<count($sql_elements);$i++) {
                if ($sql_elements[$i] == "INNER_JOIN") {
                    $sql_tables[] = $sql_elements[$i+1];

                    $foreignTable = $sql_elements[$i+1];
                    $columnInfos = $this->conn->query("SHOW FULL COLUMNS FROM $foreignTable")->fetchAll();
                    $isCorrespondentTable = in_array($foreignTable,$this->correspondenceTable);
                    /*echo $foreignTable;
                    var_dump($columnInfos);*/

                    foreach ($columnInfos as $columnInfo) {
                        //if ($columnInfo["Field"] !== "id") {
                            $fieldTitle = $columnInfo["Comment"];
                            $fieldName = $columnInfo["Field"];
                            if ($fieldTitle == "")
                                $fieldTitle = $fieldName;
                            $tables[$foreignTable][] = [
                                "name" => $fieldName,
                                "title" => $fieldTitle,
                                "type" => $this->getColumnType($columnInfo["Type"]),
                                "ai" => $columnInfo["Extra"] == "auto_increment",
                                "key" => $columnInfo["Key"],
                                "required" => $columnInfo["Null"] == "NO",
                                "hidden" => in_array($fieldName,$hidden),
                                "correspondent" => $isCorrespondentTable,
                                "table" => $foreignTable,
                            ];
                        //}
                    }

                    //echo $foreignTable;
                    //var_dump($titles);
                }
            }
        }

        foreach ($selects as $select) {
            $selectTable = explode(".",$select)[0];
            //$select[] = explode(".",$select)[1];
            if (!isset($tables[$selectTable])) {
                $column = $this->conn->query("SHOW FULL COLUMNS FROM $selectTable");
                $isCorrespondentTable = in_array($selectTable,$this->correspondenceTable);
                while ($title = $column->fetch()) {
                    //var_dump($title["Field"]);
                    $fieldTitle = strrpos($title["Comment"], "|" !== false) ? explode("|", $title["Comment"])[0] : $title["Comment"];
                    //$fieldName = $title["Field"] == "id" ? $table : $title["Field"];
                    $fieldName = $title["Field"];
                    if ($fieldTitle == "")
                        $fieldTitle = $fieldName;


                    if (strrpos($fieldName, "_id") !== false) {
                        $foreignTable = explode("_id", $fieldName)[0];
                        if (in_array($foreignTable, $this->tableNames)) {
                            $columnName = $this->getSecondColumnName($foreignTable);
                            $columnInfo = $this->conn->query("SHOW FULL COLUMNS FROM $foreignTable WHERE Field='id';")->fetch();
                            //$sql = count($allColumns) > 1 ? "SELECT $columnName FROM $foreignTable" : "SELECT id,$columnName FROM $foreignTable";

                            $nsql = "SELECT id,$columnName FROM $foreignTable";

                            $datas = $this->conn->query($nsql)->fetchAll();

                            $this->jsonJS[] = '<script id="' . $foreignTable . '" type="application/json">' . json_encode($datas) . '</script>';
                            $this->tables[$foreignTable] = $datas;
                            $this->tableSelectInput[] = $foreignTable;

                            $fieldTitle = $columnInfo["Comment"];
                            if ($fieldTitle == "")
                                $fieldTitle = $foreignTable;
                        }
                    }





                    if (strrpos($select,"*") !== false)
                        $selectTable = explode(" ",explode("FROM ",$sql)[1])[0];
                    if (in_array("$selectTable.$fieldName",$selects) OR (strrpos($select,"*") !== false)) {
                        $tables[$selectTable][] = [
                            "name" => $fieldName,
                            "title" => $fieldTitle,
                            "type" => $this->getColumnType($title["Type"]),
                            "ai" => $title["Extra"] == "auto_increment",
                            "key" => $title["Key"],
                            "required" => $title["Null"] == "NO",
                            "hidden" => in_array($fieldName,$hidden),
                            "correspondent" => $isCorrespondentTable,
                            "table" => $selectTable,
                        ];
                    }


                }
            }

        }
        return $tables;
    }

    public function getTableResults($sql,$hidden) {

        //var_dump($sql);
        $selectString = explode(" ",$sql)[1];
        $selects = strrpos($selectString,",") === false ? [$selectString] : explode(",",$selectString);
        $columnInfos = $this->getColumnInfos($sql,$hidden);
        $tables = [];
        foreach ($columnInfos as $columnInfo) {
            for ($i=0;$i<count($columnInfo);$i++) {
                if (!in_array($columnInfo[$i]["table"],$tables))
                    $tables[] = $columnInfo[$i]["table"];
            }

        }
        $sqlForValues = $sql;
        if (strrpos($sql,".*") !== false) {
            $sqlForValues = str_replace($selects[0],"*",$sql);
        }
        $values = $this->findAll("",$sqlForValues);

        $table = str_replace(".*","",$selectString);
        $this->tables[$table] = $values;

        $titles = [];

        foreach ($selects as $select) {
            $table = explode(".",$select)[0];
            $field = explode(".",$select)[1];
            //$titles[] = $columnInfos[$table]
            foreach ($columnInfos[$table] as $columnInfo) {
                if ($columnInfo["name"] == $field ) {
                    $titles[] = $columnInfo;
                }
            }
        }
        if (strrpos($selects[0],"*") !== false) {
            $table = explode(" ",explode("FROM ",$sql)[1])[0];
            foreach ($columnInfos[$table] as $columnInfo) {
                $titles[] = $columnInfo;
            }
        }
        if (strrpos($sql,"INNER JOIN") !== false) {
            $fake_sql = str_replace("INNER JOIN","INNER_JOIN",$sql);
            $sql_elements = explode(" ",$fake_sql);
            for ($i=0;$i<count($sql_elements);$i++) {
                if ($sql_elements[$i] == "INNER_JOIN") {
                    $foreignTable = $sql_elements[$i+1];
                    //$this->tables[$foreignTable] = $datas;

                    $columnName = $this->getSecondColumnName($foreignTable);
                    $nsql = "SELECT id,$columnName FROM $foreignTable";

                    $datas = $this->conn->query($nsql)->fetchAll();

                    $this->jsonJS[] = '<script id="' . $foreignTable . '" type="application/json">' . json_encode($datas) . '</script>';
                    $this->tables[$foreignTable] = $datas;

                    //if (!$this->isCorrespondenceTable($foreignTable)) {
                        foreach ($columnInfos[$foreignTable] as $columnInfo) {
                            $titles[] = $columnInfo;
                        }
                    //}

                }
            }

        }
       /* var_dump([
            "titles" => $titles,
            "values" => $values,
            "table" => $table,
        ]);*/
        return [
            "titles" => $titles,
            "values" => $values,
            "table" => $table,
            "tables" => $tables,
        ];

    }

    public function getTablesInDatabase($db): array
    {
        $schema_tables = $this->conn->query("SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA = '$db'");
        $output = [];
        while ($schema = $schema_tables->fetch()) {
            if (!$this->isCorrespondenceTable($schema[0]))
                $output[] = $schema[0]; //["TABLE_SCHEMA"];
            else {
                if (!in_array($schema[0],$this->correspondenceTable)) {
                    $this->correspondenceTable[] = $schema[0];
                }
            }
        }
        return $output;
    }

    public function isCorrespondenceTable($tablename) {
        $infos = $this->conn->query("SHOW FULL COLUMNS FROM $tablename")->fetchAll();
        $cond = true;
        foreach ($infos as $info) {
            if ($info["Key"] != "PRI") {
                $cond = false;
                break;
            }
        }
        return $cond;
    }

    public function findAll($table, $sql = ""): array
    {
        if ($sql == "")
            $sql = "SELECT * FROM $table";
        $sql_words = explode(" ",$sql);
        foreach ($sql_words as $sql_word) {
            if (strrpos($sql_word,".*") !== false) {
                $sql = str_replace($sql_word,"*",$sql);
                /*$table = explode(".",$sql_word)[0];
                $sql = str_replace("TABLE",$table,$sql);*/
            }
        }
        $datas = $this->conn->query($sql)->fetchAll();
        $output = [];

        foreach ($datas as $data) {
            $index = 0;
            /*foreach ($data as $key => $value) {
                if (!is_int($key)) {
                    if (strrpos($key, "_id") !== false) {
                        $table = explode("_id", $key)[0];
                        $name = $this->getSecondColumnName($table);
                        $nvalue = $this->conn->query("SELECT * FROM $table WHERE id = $value")->fetch();
                        $data[$index] = '<a class="selectlink" data-ref="' . $value . '" data-json="' . htmlentities(json_encode($nvalue), ENT_QUOTES, 'UTF-8') . '" href="#">' . $nvalue[$name] . '</a>';
                    }
                    $index++;
                }
            }*/
            $output[] = $data;
        }
        //var_dump($output);
        return $output;
    }

    public function select($table, $column, $where, $rel = false)
    {
        $datas = $this->dataToSql_arr($where);
        $where = $datas["setWithAliasses"];
        $params = $datas["array"];
        $sql = "SELECT $column FROM $table WHERE $where";
    }

    public function findBy($table, $column, $sql_params = "", $rel = false)
    {
        $output = [];
        $sql = "SELECT $column FROM $table";
        if ($sql_params !== "")
            $sql .= " $sql_params";
        $datas = $this->conn->prepare($sql);
        $datas->execute();
        while ($data = $datas->fetch()) {
            $output[] = $data;
        }
        return $output;
    }

    public function getColumnType($dbType,$dbValue=null)
    {
        return $dbType;
    }

    public function getSecondColumnName($table)
    {
        $titles = $this->conn->query("SHOW FULL COLUMNS FROM $table")->fetchAll();
        foreach ($titles as $title) {
            if ($title["Field"] != "id")
                return $title["Field"];
        }
        return $titles[0]["Field"];
    }

    public function getColumns($table, $selects = [])
    {
        $output = [];
        $select = [];
        $allColumns = [];
        $allColumns[$table] = $this->conn->query("SHOW FULL COLUMNS FROM $table");
        foreach ($selects as $thisSelect) {

           /* if (strrpos($thisSelect,".") === false) {
                $thisSelect = $table.".".$thisSelect;
            }
            $selectTable = explode(".",$thisSelect)[0];
            $select[] = explode(".",$thisSelect)[1];
            if (!isset($allColumns[$selectTable]))
                $allColumns[$selectTable] = $this->conn->query("SHOW FULL COLUMNS FROM $selectTable");*/

            if (strrpos($thisSelect,".") !== false) {
                $selectTable = explode(".",$thisSelect)[0];
                $select[] = explode(".",$thisSelect)[1];
                if ($selectTable!= $table && !isset($allColumns[$selectTable]))
                    $allColumns[$selectTable] = $this->conn->query("SHOW FULL COLUMNS FROM $selectTable");
            }
        }

        foreach ($allColumns as $table => $queryResult) {
            while ($title = $queryResult->fetch()) {
                //var_dump($title["Field"]);
                if (!(count($select) > 0) || in_array($title["Field"], $select)) {
                    $fieldTitle = strrpos($title["Comment"], "|" !== false) ? explode("|", $title["Comment"])[0] : $title["Comment"];
                    //$fieldName = $title["Field"] == "id" ? $table : $title["Field"];
                    $fieldName = $title["Field"];
                    if ($fieldTitle == "")
                        $fieldTitle = $fieldName;

                    if (strrpos($fieldName, "_id") !== false) {
                        $foreignTable = explode("_id", $fieldName)[0];
                        if (in_array($foreignTable, $this->tableNames)) {
                            $columnName = $this->getSecondColumnName($foreignTable);
                            $columnInfo = $this->conn->query("SHOW FULL COLUMNS FROM $foreignTable WHERE Field='id';")->fetch();
                            $sql = count($allColumns) > 1 ? "SELECT $columnName FROM $foreignTable" : "SELECT id,$columnName FROM $foreignTable";
                            $datas = $this->conn->query($sql)->fetchAll();
                            $this->jsonJS[] = '<script id="' . $foreignTable . '" type="application/json">' . json_encode($datas) . '</script>';
                            $this->tables[$foreignTable] = $datas;
                            $this->tableSelectInput[] = $foreignTable;
                            $fieldTitle = $columnInfo["Comment"];
                            if ($fieldTitle == "")
                                $fieldTitle = $foreignTable;

                        }
                    }
                    $fieldTitle=$table.".".$fieldTitle;

                    $output[] = [
                        "name" => $fieldName,
                        "title" => $fieldTitle,
                        "type" => $this->getColumnType($title["Type"]),
                        "ai" => $title["Extra"] == "auto_increment",
                        "key" => $title["Key"],
                        "required" => $title["Null"] == "NO",
                    ];
                }
            }
        }

        /*while ($title = $allColumns[$table]->fetch()) {
            if (!(count($select) > 0) || in_array($title["Field"], $select)) {
                $fieldTitle = strrpos($title["Comment"], "|" !== false) ? explode("|", $title["Comment"])[0] : $title["Comment"];
                //$fieldName = $title["Field"] == "id" ? $table : $title["Field"];
                $fieldName = $title["Field"];
                if ($fieldTitle == "")
                    $fieldTitle = $fieldName;

                if (strrpos($fieldName, "_id") !== false) {
                    $foreignTable = explode("_id", $fieldName)[0];
                    if (in_array($foreignTable, $this->tableNames)) {
                        $columnName = $this->getSecondColumnName($foreignTable);
                        $columnInfo = $this->conn->query("SHOW FULL COLUMNS FROM $foreignTable WHERE Field='id';")->fetch();
                        $datas = $this->conn->query("SELECT id,$columnName FROM $foreignTable")->fetchAll();
                        $this->jsonJS[] = '<script id="' . $foreignTable . '" type="application/json">' . json_encode($datas) . '</script>';
                        $this->tables[$foreignTable] = $datas;
                        $this->tableSelectInput[] = $foreignTable;
                        $fieldTitle = $columnInfo["Comment"];
                        if ($fieldTitle == "")
                            $fieldTitle = $foreignTable;

                    }
                }

                $output[] = [
                    "name" => $fieldName,
                    "title" => $fieldTitle,
                    "type" => $this->getColumnType($title["Type"]),
                    "ai" => $title["Extra"] == "auto_increment",
                    "key" => $title["Key"],
                    "required" => $title["Null"] == "NO",
                ];
            }
        }*/
        //var_dump($output);
        return $output;
    }

    #[ArrayShape(["col" => "string", "val" => "string", "set" => "string", "array" => "array", "aliasses" => "string", "setWithAliasses" => "string"])]
    public function dataToSql_arr($data, $keepId = true)
    {
        $data = $this->sanitize($data);
        $columns = [];
        $values = [];
        $set = [];
        $setWithAliasses = [];
        $aliasses = [];
        $array = [];
        foreach ($data as $key => $value) {
            if ($key == "password")
                $value = password_hash($value, PASSWORD_BCRYPT, ['cost' => 12]);
            if ($key != "id" && !$keepId) {
                $columns[] = $key;
                $values[] = "'" . $value . "'";
                $set[] = "$key = '$value'";
                $setWithAliasses[] = "$key = :$key";
                $aliasses[] = ":$key";
                $array[$key] = $value;
            } else {
                $array[$key] = $value;
            }
        }
        return [
            "col" => join(", ", $columns),
            "val" => join(", ", $values),
            "set" => join(", ", $set),
            "array" => $array,
            "aliasses" => join(", ", $aliasses),
            "setWithAliasses" => join(", ", $setWithAliasses),
        ];
    }

    public function execute($sql, $params, $relocation = null): array
    {
        $datas = $this->conn->prepare($sql);
        $datas->execute($params);
        if (!is_null($relocation))
            header($relocation);
        $output = [];
        while ($data = $datas->fetch()) {
            $output[] = $data;
        }
        return $output;
    }

    public function add($table)
    {
        unset($_POST["id"]);
        $datas = $this->dataToSql_arr($_POST, false);
        $columns = $datas["col"];
        $aliasses = $datas["aliasses"];
        $set = $datas["set"];
        if ($set != "") {
            $sql = "INSERT INTO $table ($columns) VALUES ($aliasses)";
            $this->execute($sql, $datas["array"], $this->location);
        }
    }

    public function edit($table)
    {

        // isset($_POST["id"])
        $_POST[$table]["id"] = $_POST["id"];
        unset($_POST["id"]);

        foreach ($_POST as $thistable => $post) {
            $datas = $this->dataToSql_arr($post, false);
            $setWithAliasses = $datas["setWithAliasses"];
            if ($setWithAliasses != "") {
                $sql = "UPDATE $thistable SET $setWithAliasses  WHERE id = :id";
                var_dump($thistable);
                var_dump($datas);
                //$this->execute($sql, $datas["array"], $this->location);
            }
        }


        /*die("");
        $datas = $this->dataToSql_arr($_POST, false);
        $setWithAliasses = $datas["setWithAliasses"];
        if ($setWithAliasses != "") {
            $sql = "UPDATE $table SET $setWithAliasses  WHERE id = :id";
            $this->execute($sql, $datas["array"], $this->location);
        }*/
    }

    public function delete($table)
    {
        $this->execute("DELETE FROM $table WHERE id = :id", ['id' => $_POST['id']], $this->location);
    }

    public function doActions($table)
    {
        $table_id = isset($_GET["t"]) ? intval($_GET["t"]) : null;
        if (!is_null($table_id) && is_int($table_id)) {
            if ($table_id < count($this->tableNames))
                $table = $this->tableNames[$table_id];
        }

        if (isset($_POST['id'])) {
            if ($_POST['id'] == "new") {
                $this->add($table);
            } elseif (strrpos($_POST['id'], "delete-") !== false) {
                $_POST["id"] = str_replace("delete-", "", $_POST["id"]);
                $this->delete($table);
            } else {
                $this->edit($table);
            }
        }
    }

    public function sanitize($garbage)
    {
        if (is_array($garbage)) {
            foreach ($garbage as $dust) {
                $dust = htmlentities($dust);
            }
        } else {
            $garbage = htmlentities($garbage);
        }
        return $garbage;
    }
}