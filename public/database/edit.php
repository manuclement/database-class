<?php
/**
 * @var array $columns
 * @var string $table
 * @var array $jsIds
 * @var array $tables
 * @var array $titlesAndValues
 */

//var_dump($tables);
?>
<div id="crud-edit-<?= $table; ?>" class="crud-edit">
    <form method="post">
        <input id="idinput" type="hidden" name="id">
        <?php foreach ($columns as $column) : ?>
            <?php if ($column["name"] !== "id") : ?>
                <div class="popup-title">
                    <?php
                    //if (in_array(str_replace("_id","",$column["name"]),$jsIds)) {
                    //if (!$column["correspondent"]) {
                        if (strrpos($column["name"],"_id")) {
                            $tableindex = str_replace("_id","",$column["name"]);
                            $name = $column["table"].'['.$column["name"].']';
                            if ($titlesAndValues["table"] != $tableindex && in_array($tableindex,$titlesAndValues["tables"])) {
                                echo '<select class="swal2-select" name="'.$name.'">';
                                foreach ($tables[$tableindex] as $thistable) {
                                    echo '<option id="'.$column["name"].'_'.$thistable[0].'" value="'.$thistable[0].'">'.$thistable[1].'</option>';
                                }

                                echo '</select>';
                                //echo '<input type="hidden" name="'.$column["table"].'[id]">';
                            }

                        }
                        else {
                            if ($column["table"] == $titlesAndValues["table"]) {
                                $required = $column["required"] ? " required" : "";
                                $name = $column["table"].'['.$column["name"].']';
                                echo DatabaseGetInputTypeToString($column["type"],$name,$required,$column["title"],"edit");
                                /*                        <input class="swal2-input" type="text" placeholder="<?= $column["title"]; ?>" name="<?= $column["name"]; ?>">*/
                            }
                        }
                    //}
                    ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </form>
</div>