<?php
/**
 * @var array $columns
 * @var string $table
 */
?>
<div id="crud-delete-<?php echo $table; ?>" class="crud-delete">
    <form method="post">
        <input type="hidden" name="delete">
        <input type="hidden" name="id">
        <?php foreach ($columns as $column) : ?>
            <?php if ($column["name"] !== "id") : ?>
                <div class="popup-title">
                    <input class="swal2-input" type="text" placeholder="<?php echo $column["title"]; ?>" name="<?php echo $column["name"]; ?>">
                    <div>
                        <?php echo $column["title"]; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </form>
</div>