<?php
/**
 * @var array $javascripts
 * @var string $table
 * @var array $actions
 * @var boolean $hasCrudActions
 * @var array $titlesAndValues
 */

$columns = $titlesAndValues["titles"];
$tableValues = $titlesAndValues["values"];
//var_dump($tableValues);
function DatabaseGetInputTypeToString($dbType,$name,$required,$title,$type) {

    //var_dump([$dbType,$name,$required,$title,$type]);

    if ($dbType == "date" || $dbType == "datetime") {
        return '<input placeholder="'.$title.'" name="'.$name.'" type="datetime"'.$required.'>';
    }
    elseif ($dbType == "time") {
        return '<input placeholder="'.$title.'" name="'.$name.'" type="datetime"'.$required.'>';
    }
    elseif ($name == "password") {
        return $type == "edit" ? '' : '<input class="swal2-input" placeholder="'.$title.'" name="'.$name.'" type="password"'.$required.'>';
    }
    elseif ($name == "email") {
        return '<input placeholder="'.$title.'" class="swal2-input" name="'.$name.'" type="email"'.$required.'>';
    }
    elseif ($dbType == "timestamp") {
        return '<input placeholder="'.$title.'" class="swal2-input" type="text" disabled style="background-color: #f2f2f2"'.$required.'>';
    }
    elseif (strrpos($dbType,"int(") !== false || strrpos($dbType,"tinyint(") !== false || strrpos($dbType,"bigint(") !== false || strrpos($dbType,"mediumint(") !== false) {
        preg_match('#\((.*?)\)#', $dbType, $match);
        return '<input placeholder="'.$title.'" class="swal2-input" name="'.$name.'" type="text" pattern="\d*" maxlength="'.$match[1].'"'.$required.'>';
    }
    elseif (strrpos($dbType,"varchar(") !== false) {
        preg_match('#\((.*?)\)#', $dbType, $match);
        return '<input placeholder="'.$title.'" class="swal2-input" name="'.$name.'" type="text" maxlength="'.$match[1].'"'.$required.'>';
    }
    elseif (strrpos($dbType,"decimal(") !== false) {
        preg_match('#\((.*?)\)#', $dbType, $match);
        $int = explode(",",$match[1])[0];
        $decimal = explode(",",$match[1])[1];
        return '<input placeholder="'.$title.'" class="swal2-input" name="'.$name.'" type="text" pattern="\d*" maxlength="'.$match[1].'"'.$required.'>';
    }
    elseif ($dbType == "boolean") { // TODO : Deal with boolean columns
        return '<input class="swal2-checkbox" name="'.$name.'" type="checkbox"'.$required.'>';
    }
    elseif ($dbType == "text") {
        //return '<textarea class="swal2-textarea"></textarea>';
        return '<input type="text" class="swal2-input" name="'.$name.'">';
    }
}

require("add.php");
require("edit.php");

foreach ($javascripts as $javascript) {
    echo $javascript;
}

function DatabaseDisplayValue($dbType,$dbValue) {
    $output = $dbValue;
    if ($dbType == "date" || $dbType == "datetime")
        return date("Y-m-d",strtotime($dbValue));
    elseif ($dbType == "timestamp") {
        return date("Y-m-d",$dbValue);
    }
    return htmlentities($output, ENT_QUOTES, 'UTF-8');
}

function DatabaseDisplayResult($dbType,$dbValue) {
    if ($dbType == "date" || $dbType == "datetime") {
//        return $dbType == "date" ? date("d/m/Y",strtotime($dbValue)) : date("d/m/Y - H:i",strtotime($dbValue));
        return $dbType == "date" ? date("d/m/Y",strtotime($dbValue)) : date("d/m/Y - H:i",strtotime($dbValue));
    }

    elseif ($dbType == "timestamp") {
        return date("d/m/Y - H:i",$dbValue);
    }

    elseif ($dbType == "boolean") {
        return $dbValue ? '<span>&#9745;</span>' : '<span>&#9746;</span>';
    }

    else return $dbValue;

    /*if ($dbType == "date" || $dbType == "datetime") {
        $date = $dbType == "date" ? date("d/m/Y",strtotime($dbValue)) : date("d/m/Y - H:I:s",strtotime($dbValue));
        //return 'type="date" data-value="'.$date.'"';
        return [
            "tag" => "input",
            "attributes" => [
                "type" => "date",
                "data-value" => $date,
            ],
        ];
    }

    elseif ($dbType == "timestamp") {
        $date = date("d/m/Y - H:I:s",$dbValue);
        //return 'type="date" data-value="'.$date.'"';
        return [
            "tag" => "input",
            "attributes" => [
                "type" => "date",
                "data-value" => $date,
            ],
        ];
    }


    elseif (strrpos($dbType,"int(") !== false || strrpos($dbType,"tinyint(") !== false || strrpos($dbType,"bigint(") !== false || strrpos($dbType,"mediumint(") !== false) {
        preg_match('#\((.*?)\)#', $dbType, $match);
        //return 'type="text" pattern="\d*" maxlength="'.$match[1].'"';
        return [
            "tag" => "input",
            "attributes" => [
                "type" => "text",
                "pattern" => "\d*",
                "maxlength" => $match[1],
                "data-value" => $dbValue,
            ],
        ];
    }

    elseif (strrpos($dbType,"varchar(") !== false) {
        preg_match('#\((.*?)\)#', $dbType, $match);
        //return 'type="text" maxlength="'.$match[1].'"';
        return [
            "tag" => "input",
            "attributes" => [
                "type" => "text",
                "maxlength" => $match[1],
                "data-value" => $dbValue,
            ],
        ];
    }

    elseif (strrpos($dbType,"decimal(") !== false) {
        preg_match('#\((.*?)\)#', $dbType, $match);
        $int = explode(",",$match[1])[0];
        $decimal = explode(",",$match[1])[1];
        //return 'type="text" data-value="decimal-'.$int.'-'.$decimal.'" pattern="\d*" maxlength="'.$match[1].'"';
        return [
            "tag" => "input",
            "attributes" => [
                "type" => "text",
                "data-value" => "decimal|$int|$decimal|$dbValue",
                "pattern" => "\d*",
                "maxlength" => $match[1],
            ],
        ];
    }

    elseif ($dbType == "boolean") {
        $attributes = [
            "type" => "checkbox",
            "data-value" => false,
        ];

        if ($dbValue == 1) {
            $attributes["checked"] = true;
            $attributes["data-value"] = true;
        }

        return [
            "tag" => "input",
            "attributes" => $attributes,
        ];
    }

    else {
        return [
            "tag" => "input",
            "attributes" => [
                "type" => "text",
                "data-value" => $dbValue,
            ],
        ];
    }*/
}

function DatabaseGeInputValue($input) {
    $value = $input["attributes"]["data-value"];
    return $value;
}

?>

<table class="crud-table">
    <thead>
    <tr>
        <?php $th_index = 0; foreach ($columns as $column) : ?>
            <?php if (!$column["hidden"]) : ?>
                <th><?= $column["title"]; ?></th>
            <?php endif; ?>
        <?php $th_index++; endforeach; ?>
        <?php if ($hasCrudActions) : ?>
            <th>&nbsp;</th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($tableValues as $tableValue) : ?>
        <tr class="crud-line" id="<?=$tableValue["id"]?>">
            <?php for ($i = 0; $i<count($columns);$i++) :
                $style = $columns[$i]["hidden"] ? 'style="display:none" ' : '';
                $index = $columns[$i]["name"];
                $tableIndex = $columns[$i]["table"]."_id";
                $dataId = isset($tableValue[$tableIndex]) ? $tableValue[$tableIndex] : "null";
                ?>
<!--                --><?php //if (isset($tableValue[$i])) : ?>
                    <td <?=$style?>class="crud-input"
                        data-id="<?= $dataId ?>"
                        data-t="<?= $columns[$i]["table"]."_id" ?>"
                        data-value="<?= DatabaseDisplayValue($columns[$i]["type"],$tableValue[$i]) ?>"
                        data-required="<?= $columns[$i]["required"]; ?>"
                        data-name="<?= $columns[$i]["table"]; ?>[<?= $columns[$i]["name"]; ?>]"><?= DatabaseDisplayResult($columns[$i]["type"],$tableValue[$i]); ?>
                    </td>
<!--                --><?php //endif; ?>
            <?php endfor; ?>
            <?php if ($hasCrudActions) : ?>
                <td class="crud-actions">
                    <a href="#" id="crudbutton-edit-<?= $table.'-'.$tableValue['id']; ?>"><img style="width:20px" src="database/edit.svg" alt="Editer"></a>
                    <?php foreach ($actions as $action) echo str_replace("[ID]",$tableValue['id'],$action["html"]); ?>
                </td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>

</table>
<?php if ($hasCrudActions) : ?>
    <div class="row"><button id="crudbutton-add-<?= $table; ?>-new" class="crudButton">Ajouter</button></div>
<?php endif; ?>