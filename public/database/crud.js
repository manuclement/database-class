const crudLaunchers = document.querySelectorAll(".crudButton, .crud-actions a");
const crudSelectLinks = document.querySelectorAll(".selectlink");
for (let crudSelectLink of crudSelectLinks) {
    crudSelectLink.addEventListener("click", function (event) {
        let html = '';
        const obj = JSON.parse(event.currentTarget.dataset.json);
        for (const [key, value] of Object.entries(obj)) {
            if (isNaN(key)) {
                html += '<div style="text-align:left"><strong>' + key + '</strong>: ' + value + '</div>';
            }
        }
        Swal.fire({
            title: 'Data',
            html: html
        });
        event.preventDefault();
    });
}
for (let crudLauncher of crudLaunchers) {
    crudLauncher.addEventListener("click", function (event) {
        const target = event.currentTarget;
        const action = target.id.split("-")[1];
        const table = target.id.split("-")[2];
        const id = target.id.split("-")[3];
        const html = document.getElementById("crud-" + action + "-" + table).innerHTML;
        const title = action === "add" ? "Ajouter une nouvelle ligne" : 'Modifier cette ligne';

        const Swal_options = {
            title: title,
            html: html,
            showCancelButton: true,
            showDenyButton: false,
            confirmButtonText: "Ajouter",
            cancelButtonText: "Annuler",
            denyButtonText: `Supprimer`,
            preConfirm: submitCrud,
            didOpen: () => {
                if (action === "edit") {
                    const tds = target.parentNode.parentNode.querySelectorAll("td");
                    document.querySelector('.swal2-html-container input[name="id"]').value = id;
                    for (let td of tds) {
                        if (!td.classList.contains("crud-actions")) {
                            //console.log(td.dataset.id)
                            if (td.dataset.id !== "null") {
                                //document.querySelector('.swal2-html-container option[id="' + td.childNodes[0].dataset.ref + '"]').selected = 'selected';
                                idtofind = td.dataset.t + '_' + td.dataset.id;
                                console.log(idtofind)
                                if (document.querySelector('.swal2-html-container option[id="'+idtofind+'"]'))
                                    document.querySelector('.swal2-html-container option[id="'+idtofind+'"]').selected = 'selected';
                            } else {
                                //console.log(document.querySelector('.swal2-html-container input[name="' + td.dataset.name + '"]'),td.dataset.name,td.dataset.value)
                                if (document.querySelector('.swal2-html-container input[name="' + td.dataset.name + '"]'))
                                    document.querySelector('.swal2-html-container input[name="' + td.dataset.name + '"]').value = td.dataset.value;
                            }
                        }
                    }
                }
            },
            preDeny: () => {
                document.querySelector('.swal2-html-container input[name="id"]').value = "delete-" + id;
                submitCrud();
            }
        };

        if (action === "edit") {
            Swal_options.confirmButtonText = "Modifier";
            Swal_options.showDenyButton = true;
        }

        Swal.fire(Swal_options);
        event.preventDefault();
    });
}

function submitCrud() {
    if (document.getElementById("swal2-html-container")) {
        const form = document.getElementById("swal2-html-container").querySelector("form");
        const inputs = form.querySelectorAll("input");
        let error_reason = "";
        console.log(inputs)
        for (let input in inputs) {
            console.log(input)
            if (input.required && input.value == "") {
                error_reason = input.name;
                console.log(error_reason)
            }

        }
        if (error_reason !== "") {
            console.log(error_reason)
            Swal.showValidationMessage(
                `Veuillez remplir tous les champs (${error_reason})`
            );
            return false;
        }
        if (form)
            form.submit();
    }
}