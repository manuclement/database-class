<?php

if (!isset($database)) {
    require("classes/Connection.php");
    $database = new Connection();
}

// si l'utilisateur est déjà connecté on redirige vers la page d'accueil
if ( isset($sessionData) ) {
    header($database->location);
}

// si l'utilisateur se connecte: vérification du login et du mot de passe
if (isset($_POST['login']) && isset($_POST['password'])
    && !empty($_POST['login']) && !empty($_POST['password'])) {

    $user = $database->conn->prepare("SELECT * FROM user WHERE username = :username");
    $user->execute([
        'username' => $_POST['login'],
    ]);

    if ($user->rowCount() > 0) {
        $user = $user->fetch();

        // vérification du mot de passe
        if ( password_verify($_POST['password'], $user['password'])) {
            // on garde en session l'id de l'utilisateur connecté
            $_SESSION['user_id'] = $user['id'];
            header('Location: index.php');
        }
    }

}

// rendu de la page
echo $twig->render('pages/login.html.twig', [
    'sessionData' => $sessionData,
]);


