<?php

$db_connections = [
    "dev" => [
        "server_host" => "localhost",
        "server_port" => 8080,
        "db_host" => "localhost",
        "db_port" => -1,
        "db_name" => "db_attens",
        "db_username" => "eleve",
        "db_password" => "bonjour"
    ],
    "prod" => [
        "server_host" => "prod.com",
        "server_port" => -1,
        "db_host" => "localhost",
        "db_port" => -1,
        "db_name" => "db_agenda",
        "db_username" => "eleve",
        "db_password" => "bonjour"
    ],
];