<?php
ini_set('display_errors',1);

require("database/classes/Database.php");

$database = new Database();
$database->doActions("file");
$database->checkSession();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="database/sweetalert2.js"></script>
    <link rel="stylesheet" href="database/mvp.css">
    <link rel="stylesheet" href="database/crudstyle.css">
    <title>Demo CRUD avec PDO</title>
</head>
<body>

<?php

/*$database->makeCrud("ligne_commande", [
        "sql" => "
SELECT ligne_commande.date_livraison,ligne_commande.adresse_livraison,concat(client.nom,client.prenom)as nom,fleur.id_fleur FROM ligne_commande 
INNER JOIN commande ON ligne_commande.id = commande.id 
INNER JOIN client ON commande.client_id = client.id 
INNER JOIN fleur ON ligne_commande.id_fleur = fleur.id_fleur 
WHERE ligne_commande.date_livraison < CURRENT_DATE()
"
]);*/

/*$database->makeCrud("client", [
    'actions' => [
        'commande' => 'voir les commandes',
    ],
    'hide' => 'id',
]);*/

/*$database->makeCrud("client",[
    'sql' => 'SELECT client.id,client.nom,client.prenom,client.adresse,client.code_postal,client.ville FROM client LIMIT 5',
    'hide' => 'id',

]);

$database->makeCrud("commande",[
    'sql' => "
SELECT ligne_commande.id,ligne_commande.date_livraison,ligne_commande.adresse_livraison,client.nom,client.prenom 
FROM ligne_commande 
INNER JOIN commande ON ligne_commande.id = commande.id 
INNER JOIN client ON commande.client_id = client.id
    ",
]);*/

/*$database->makeCrud("commande",[
    //'sql' => "SELECT commande.id,client.nom date_commande FROM commande INNER JOIN client ON commande.client_id = client.id"
    'sql' => "
SELECT client.nom,client.prenom,commande.id,commande.date_commande
FROM commande
INNER JOIN client ON commande.client_id = client.id
LIMIT 5",
]);*/

/*$database->makeCrud("ligne_commande",[
    //'sql' => "SELECT commande.id,client.nom date_commande FROM commande INNER JOIN client ON commande.client_id = client.id"
    'sql' => "
SELECT ligne_commande.id,ligne_commande.date_livraison,ligne_commande.adresse_livraison
FROM ligne_commande 
LIMIT 5",
]);*/


$database->makeCrud("file", [
    "hide" => ["id","role_id","password","file_id","long_description","street","user_id","client_id","zip","city","phone","mobile","email","username","client_street","client_city","client_zip","country_id","client_email","client_phone"],
    "sql" => 'SELECT * 
FROM file
INNER JOIN user ON file.user_id = user.id
INNER JOIN client ON file.client_id = client.id'
]);

//$database->makeCrud("user");

?>

</body>
</html>
